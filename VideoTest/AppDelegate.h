//
//  AppDelegate.h
//  VideoTest
//
//  Created by Oleg Chebotarev on 20.12.16.
//  Copyright © 2016 Oleg Chebotarev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

