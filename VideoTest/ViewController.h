//
//  ViewController.h
//  VideoTest
//
//  Created by Oleg Chebotarev on 20.12.16.
//  Copyright © 2016 Oleg Chebotarev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Affdex/Affdex.h>

@interface ViewController : UIViewController <AFDXDetectorDelegate,
    UIImagePickerControllerDelegate> 
@property (weak, nonatomic) IBOutlet UIButton *action;

- (IBAction)selectVideo:(id)sender;

@end

