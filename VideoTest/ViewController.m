//
//  ViewController.m
//  VideoTest
//
//  Created by Oleg Chebotarev on 20.12.16.
//  Copyright © 2016 Oleg Chebotarev. All rights reserved.
//

#import "ViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <AVKit/AVKit.h>
#import <CoreImage/CoreImage.h>
#import <CoreMotion/CoreMotion.h>


@interface ViewController ()

@end

@implementation ViewController {
    AFDXDetector* detector;
    UIImage* firstImage;
    UIImage* secondImage;
    UIImage* mask;
    BOOL inProcess;
    NSMutableArray* map;
    AVAssetWriter* videoWriter;
    AVAssetWriterInput* writerInput;
    AVAssetWriterInputPixelBufferAdaptor* adapter;
    
    CVPixelBufferRef buffer;
    
    NSString* path;
    
    float frameRate;
    
    int counter;
    int errorCounter;
    
    int width;
    int height;
    float maskAspect;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    firstImage = [UIImage imageNamed:@"face_one"];
    secondImage = [UIImage imageNamed:@"face_two"];
    mask = [UIImage imageNamed:@"mask_new"];
    maskAspect = [mask size].height / [mask size].width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/**
 PREPARING
 */
    


- (IBAction)selectVideo:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,      nil];
    
    [self presentModalViewController:imagePicker animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];
        
        AVAssetTrack* videoTrack = nil;
        AVURLAsset *asset = [AVURLAsset assetWithURL:videoUrl];
        NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
        
        if ([videoTracks count] > 0)
            videoTrack = [videoTracks objectAtIndex:0];
        frameRate = [videoTrack nominalFrameRate];
        [self initDetectorWithFilePath:moviePath];
    }
    
    [self dismissModalViewControllerAnimated:YES];
}



/**
 DETECTOR 
 */




- (void)initDetectorWithFilePath:(NSString*)file {
    [_action setEnabled:NO];
    buffer = NULL;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    map = [NSMutableArray new];
    detector = [[AFDXDetector alloc] initWithDelegate:self usingFile:file maximumFaces:3 faceMode:LARGE_FACES];
    [detector start];
    
}
    
- (void) detector:(AFDXDetector *)detector didStopDetectingFace:(AFDXFace *)face {
    
}

    
- (void) detector:(AFDXDetector *)detector didStartDetectingFace:(AFDXFace *)face {
    
}


    
- (void) detector:(AFDXDetector *)detector hasResults:(NSMutableDictionary *)faces forImage:(UIImage *)image atTime:(NSTimeInterval)time {
    if(!inProcess) {
        inProcess = YES;
        [self prepare:image];
    }
    
    if(faces == nil) return;
    image = [self drawFaces:faces onImage:image];
    [self appendImage:image];
}






/**
 
 INITIALIZATION
 */

- (void)prepare:(UIImage*)image {
    counter = 0;
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    width = size.width;
    height = size.height;
    NSString *fileNameOut = @"temp.mp4";
    NSString *directoryOut = @"tmp/";
    NSString *outFile = [NSString stringWithFormat:@"%@%@",directoryOut,fileNameOut];
    path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", outFile]];
    NSURL *videoTempURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), fileNameOut]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[videoTempURL path]  error:NULL];
    
    
    NSError *error = nil;
    
    videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:path]
                                                           fileType:AVFileTypeQuickTimeMovie
                                                              error:&error];
    
    
    NSParameterAssert(videoWriter);
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:size.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:size.height], AVVideoHeightKey,
                                   nil];
    
    writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                                         outputSettings:videoSettings];
    
    adapter = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                                                                                                     sourcePixelBufferAttributes:nil];
    NSParameterAssert(writerInput);
    NSParameterAssert([videoWriter canAddInput:writerInput]);
    [videoWriter addInput:writerInput];
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
}


/**
 VIDEO PROCESSING
 
 */



- (void)appendImage:(UIImage*)image {
    if(counter > 300) return;
    if(image == nil) return;
    if(writerInput.readyForMoreMediaData){
        errorCounter = 0;
        CMTime frameTime = CMTimeMake(600 / frameRate, 600);
        CMTime lastTime=CMTimeMake(counter * 600 / frameRate, 600);
        CMTime presentTime=CMTimeAdd(lastTime, frameTime);
        
        if (counter == 0) {presentTime = CMTimeMake(0, 600);}
        
        buffer = [self pixelBufferFromCGImage:[image CGImage]];
        [adapter appendPixelBuffer:buffer withPresentationTime:presentTime];
        counter++;
    } else {
        if(errorCounter++ == 20) return;
        [self appendImage:image];
    }

}

- (void) finishVideoSession {
    [writerInput markAsFinished];
    [videoWriter finishWritingWithCompletionHandler:^{
        if (videoWriter.status != AVAssetWriterStatusFailed && videoWriter.status == AVAssetWriterStatusCompleted) {
            NSURL *videoTempURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", path]];
            [self saveToCameraRoll:videoTempURL];
            
        } else {
            NSLog(@"Video writing failed: %@", videoWriter.error);
        }
        
    }];
    CVPixelBufferPoolRelease(adapter.pixelBufferPool);
}


/**
 HELPERS
 
 */

- (CVPixelBufferRef) pixelBufferFromCGImage: (CGImageRef) image {
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, width,
                                          height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pxdata, width, height, 8, 4 * width, rgbColorSpace,
                                                 kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    
    return pxbuffer;
}

- (void) saveToCameraRoll:(NSURL *)srcURL {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    ALAssetsLibraryWriteVideoCompletionBlock videoWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error: %@", error );
        } else {
            AVPlayer *player = [AVPlayer playerWithURL:newURL];
            AVPlayerViewController *playerViewController = [AVPlayerViewController new];
            playerViewController.player = player;
            [self presentViewController:playerViewController animated:YES completion:nil];
        }
    };
    
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:srcURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:srcURL
                                    completionBlock:videoWriteCompletionBlock];
    }
}




/**
 HERE COMES THE DRAWING
 
 */

- (UIImage*) drawFaces:(NSDictionary*) faces onImage:(UIImage*)image {
    UIImage* rotatedMask;
    AFDXFace* face;
    if(faces != nil) for(AFDXFace* f in [faces allValues]) {
        face = f;
        rotatedMask = rotate(mask, face);
        break;
    }
    
    UIGraphicsBeginImageContextWithOptions(image.size, YES, 1.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    [image drawInRect:CGRectMake(0, 0, width, height)];
    if(rotatedMask != nil) {
        CGRect rect = [face faceBounds];
        [rotatedMask drawInRect:[self calculateBounds:rect]];
    }
//    NSMutableArray* uniques = [NSMutableArray new];
//    if(faces != nil) for(AFDXFace* face in [faces allValues]) {
//        
////        UIImage* mask;
////        if([uniques count] == 0) {
////            [uniques addObject:[NSNumber numberWithInt: [face faceId]]];
////            mask = firstImage;
////        } else {
////            mask = secondImage;
////        }
//        CGRect rect = [face faceBounds];
//        [[self rotate:face] drawInRect:[self calculateBounds:rect]];
//    }
    
    UIGraphicsPopContext();
    
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return outputImage;
}

UIImage* rotate(UIImage* src, AFDXFace* face) {
    UIGraphicsBeginImageContext(src.size);
    UIImageOrientation orientation = src.imageOrientation;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    AFDXOrientation* faceOrientation = face.orientation;
    CATransform3D fullTransform = CATransform3DIdentity;
    CGAffineTransform affine = CGAffineTransformMake(fullTransform.m11, fullTransform.m12, fullTransform.m21, fullTransform.m22, fullTransform.m41, fullTransform.m42);
    CGContextConcatCTM(context, affine);
//    CATransform3D perspectiveTransform = CATransform3DIdentity;
//    perspectiveTransform.m34 = 0.4;
//    perspectiveTransform.m33 = 0.4;
//    CATransform3DRotate(perspectiveTransform, <#CGFloat angle#>, <#CGFloat x#>, <#CGFloat y#>, <#CGFloat z#>)
    [src drawAtPoint:CGPointMake(0, 0)];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(CGRect)calculateBounds:(CGRect)faceBound {
    
    CGRect result = CGRectMake(faceBound.origin.x, faceBound.origin.y, faceBound.size.width, faceBound.size.width * maskAspect);
    return result;
}

- (void)detectorDidFinishProcessing:(AFDXDetector *)detector {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self finishVideoSession];
    [_action setEnabled:YES];
    inProcess = NO;
}

@end
